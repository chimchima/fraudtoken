pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "./FraudToken.sol";

contract GLDToken is ERC20, Ownable {
    FraudToken public fraudToken;

    constructor(uint256 _initialSupply, address _fraudToken) public ERC20("Gold", "GLD") {
        fraudToken = FraudToken(_fraudToken);
        ERC20._mint(msg.sender, _initialSupply);
    }
    
    modifier notFraud() {
        require(!fraudToken.isFraud(msg.sender), "Fraud can't call this function");
        _;
    }
   
    function mint(address _account, uint256 _amount) public onlyOwner {
        _mint(_account, _amount);
    }

    function burn(address _account, uint256 _amount) public onlyOwner {
        _burn(_account, _amount);
    }
    
    function _beforeTokenTransfer(address _from, address _to, uint256 _tokenId) internal override notFraud {
        if(_from == address(0) || _to == address(0)) {
            return;
        }
        require(!fraudToken.isFraud(_from), "you can't transfer token from the fraud");
    }

}