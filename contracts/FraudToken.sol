pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract FraudToken is ERC721, Ownable {
    uint256 private tokenCounter;
    // mapping from address to token id
    mapping(address => uint256) private frauds;

    constructor() ERC721("FraudToken", "FRD") Ownable() public {}
    
    modifier notFraud() {
        require(!isFraud(msg.sender), "Fraud can't call this function");
        _;
    }

    function isFraud(address _account) public view returns(bool) {
        return ERC721.balanceOf(_account) == 1;
    }

    function mint(address _account) public onlyOwner {
        require(!isFraud(_account), "This account already has fraud token");
        require(_account != owner(), "admin can't mark self");
        uint256 _newItemId = tokenCounter;
        ERC721._safeMint(_account, _newItemId);
        frauds[_account] = _newItemId;
        tokenCounter = _newItemId + 1;
    }
    
    function burn(address _account) public onlyOwner {
        require(isFraud(_account), "This account doesn't have fraud token");
        _burn(frauds[_account]);
    }
    
    function _beforeTokenTransfer(address _from, address _to, uint256 _tokenId) view internal override onlyOwner {}
}