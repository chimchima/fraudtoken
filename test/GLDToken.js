const GLDToken = artifacts.require('./GLDToken.sol');
const FraudToken = artifacts.require('./FraudToken.sol');
const Reverter = require("./helpers/reverter");
const truffleAssert = require('truffle-assertions');

contract("GLDToken", async (accounts) => {
    const reverter = new Reverter(web3);
    const CREATOR = accounts[0];
    const ALICE = accounts[1];
    const BOB = accounts[2];
    let fraudToken;
    let gldtoken;

    before("setup", async () => {
        fraudToken = await FraudToken.new();
        gldtoken = await GLDToken.new(10, fraudToken.address);
        reverter.snapshot();
    });

    afterEach(reverter.revert);

    describe("constructor", async () => {
        it("should init contact field", async () => {
            assert.equal(10, await gldtoken.balanceOf(CREATOR));
            assert.equal(CREATOR, await gldtoken.owner());
        });
    });

    describe("transfer, transferFrom", async () => {
        it("transfer", async () => {
            await gldtoken.transfer(ALICE, 1, {from: CREATOR});

            assert.equal(1, await gldtoken.balanceOf(ALICE));
            assert.equal(9, await gldtoken.balanceOf(CREATOR));
        });

        it("transferFrom", async () => {
            await gldtoken.transfer(ALICE, 2, {from: CREATOR});
            await gldtoken.increaseAllowance(CREATOR, 2, {from: ALICE});

            assert.equal(2, await gldtoken.allowance(ALICE, CREATOR));

            await gldtoken.transferFrom(ALICE, BOB, 1, {from: CREATOR});

            assert.equal(8, await gldtoken.balanceOf(CREATOR));
            assert.equal(await gldtoken.balanceOf(ALICE).toString(), await gldtoken.balanceOf(BOB).toString());
            assert.equal(1, await gldtoken.allowance(ALICE, CREATOR));

            await gldtoken.decreaseAllowance(CREATOR, 1, {from: ALICE});
            
            assert.equal(0, await gldtoken.allowance(ALICE, CREATOR));
        });

        it("frauds can't transfer token", async () => {
            await gldtoken.mint(ALICE, 2, {from: CREATOR});
            await fraudToken.mint(ALICE, {from: CREATOR});

            await truffleAssert.reverts(gldtoken.transfer(BOB, 1, {from: ALICE}), "Fraud can't call this function");

            await gldtoken.mint(BOB, 3, {from: CREATOR});
            await gldtoken.increaseAllowance(ALICE, 2, {from: BOB});
            
            await truffleAssert.reverts(gldtoken.transferFrom(BOB, CREATOR, 1, {from: ALICE}), "Fraud can't call this function");
        });

        it("you can't transfer token from the fraud", async () => {
            await gldtoken.mint(ALICE, 3, {from: CREATOR});
            await gldtoken.increaseAllowance(BOB, 2, {from: ALICE});
            await fraudToken.mint(ALICE, {from: CREATOR});

            await truffleAssert.reverts(gldtoken.transferFrom(ALICE, CREATOR, 1, {from: BOB}), "you can't transfer token from the fraud");
        });
    });

    describe("mint, burn", async () => {
        it("should mint 1 token", async () => {
            let balanceBeforeMint = await gldtoken.balanceOf(ALICE);
            await gldtoken.mint(ALICE, 1, {from: CREATOR});

            assert.equal(parseInt(balanceBeforeMint) + 1, await gldtoken.balanceOf(ALICE));
        });

        it("should burn 1 token", async () => {
            let balanceBeforeBurn = await gldtoken.balanceOf(CREATOR);
            await gldtoken.burn(CREATOR, 1, {from: CREATOR});
            
            assert.equal(parseInt(balanceBeforeBurn) - 1, await gldtoken.balanceOf(CREATOR));
        });

        it("only owner can mint and burn tokens", async () => {
            await truffleAssert.reverts(gldtoken.mint(ALICE, 1000, {from: ALICE}), "caller is not the owner");
            await truffleAssert.reverts(gldtoken.burn(ALICE, 1000, {from: BOB}), "caller is not the owner");
        });
    });
});