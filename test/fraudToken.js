const FraudToken = artifacts.require('./FraudToken.sol');
const Reverter = require("./helpers/reverter");
const truffleAssert = require('truffle-assertions');

contract("FraudToken", async (accounts) => {
    const reverter = new Reverter(web3);
    const CREATOR = accounts[0];
    const ALICE = accounts[1];
    const BOB = accounts[2];
    let fraudToken;

    before("setup", async () => {
        fraudToken = await FraudToken.new({from: CREATOR});
        await reverter.snapshot();
    });

    afterEach(reverter.revert);

    describe("mint, burn", async () => {
        it("should mint fraud token", async () => {
            assert.equal(false, await fraudToken.isFraud(ALICE));
            
            await fraudToken.mint(ALICE, {from: CREATOR});

            assert.equal(true, await fraudToken.isFraud(ALICE));
            await truffleAssert.reverts(fraudToken.mint(ALICE, {from: CREATOR}), "This account already has fraud token");
        });

        it("should burn fraud token", async () => {
            await fraudToken.mint(ALICE, {from: CREATOR});
            await fraudToken.burn(ALICE, {from: CREATOR});

            assert.equal(false, await fraudToken.isFraud(ALICE));
            await truffleAssert.reverts(fraudToken.burn(ALICE, {from: CREATOR}), "This account doesn't have fraud token");
        });

        it("only owner", async () => {
            await truffleAssert.reverts(fraudToken.mint(BOB, {from: ALICE}), "caller is not the owner");

            await fraudToken.mint(ALICE, {from: CREATOR});
            await truffleAssert.reverts(fraudToken.burn(ALICE, {from: ALICE}), "caller is not the owner");
        });

        it("admin can't mark self", async () => {
            await truffleAssert.reverts(fraudToken.mint(CREATOR, {from: CREATOR}), "admin can't mark self");
        });
    });

    describe("transferFrom, safeTransferFrom", async () => {
        it("transferFrom", async () => {
            await fraudToken.mint(ALICE, {from: CREATOR});
            await fraudToken.approve(BOB, 0, {from: ALICE});

            await truffleAssert.reverts(fraudToken.transferFrom(ALICE, CREATOR, 0, {from: BOB}), "caller is not the owner");
        });

        it("safeTransferFrom", async () => {
            await fraudToken.mint(ALICE, {from: CREATOR});
            await fraudToken.approve(BOB, 0, {from: ALICE});

            await truffleAssert.reverts(fraudToken.safeTransferFrom(ALICE, CREATOR, 0, {from: BOB}), "caller is not the owner");
        });
    });
});