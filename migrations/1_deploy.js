const FraudToken = artifacts.require("FraudToken");
const GLDToken = artifacts.require("GLDToken");

let fraudToken;

module.exports = async function(deployer) {
    fraudToken = await FraudToken.new();
    await GLDToken.new(10, fraudToken.address);
};
