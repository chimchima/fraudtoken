const Web3 = require('web3');
const HDWalletProvider = require('truffle-hdwallet-provider');
const infuraKey = "106d2c009ee946cfb594b5e7d827b490";

const web3 = new Web3();
const fs = require('fs');
const mnemonic = fs.readFileSync('.secret').toString().trim();
const account = "0x0b0c7c487a9237a2d6ecc2223571042052a34713";
const etherscanApiKey = "YDRDIJPCBTCPZNKWCM5RCT1Q8BS823AR3C";

module.exports = {
  // Uncommenting the defaults below 
  // provides for an easier quick-start with Ganache.
  // You can also follow this format for other networks;
  // see <http://truffleframework.com/docs/advanced/configuration>
  // for more details on how to specify configuration options!
  //
  networks: {
    development: {
      host: "127.0.0.1",
      port: 8545,
      network_id: "*"
    },
    ropsten: {
      provider: () =>
        new HDWalletProvider(
          mnemonic,
          'https://ropsten.infura.io/v3/106d2c009ee946cfb594b5e7d827b490'
        ),
      network_id: 3,
      gasPrice: 10000000000,
      gas: 8000000,
      from: account,
      confirmations: 2,
      timeoutBlocks: 200,
      skipDryRun: true,
    },
  },
  compilers: {
    solc: {
      version: '0.8.0',
      docker: false,
      settings: {
        optimizer: {
          enabled: false,
          runs: 1000,
        },
      },
    },
  },
  plugins: ["truffle-plugin-verify"],
  api_keys: {
    etherscan: etherscanApiKey,
  },

  //
};


